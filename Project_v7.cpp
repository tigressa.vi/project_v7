
#include <iostream>

using namespace std;

class stack
{
public:
	int data;
	stack* next;
};

stack* node(int data)
{
	stack * stacks = new stack();
	stacks->data = data;
	stacks->next = NULL;
	return stacks;
}

int empty(stack* root)
{
	return !root;
}

void push(stack** root, int data)
{
	stack* stacks = node(data);
	stacks->next = *root;
	*root = stacks;
	cout << data << "pushed \n";
}

int pop(stack** root)
{
	if (empty(*root))
		return INT_MIN;
	stack* temp = *root;
	*root = (*root)->next;
	int popped = temp->data;
	free(temp);
	return popped;
}
int peek(stack* root)
{
	if (empty(root))
		return INT_MIN;
	return root->data;

}

int main()
{
	stack * root = NULL;

	push(&root,10);
	push(&root,20);
	push(&root,30);

	cout << pop(&root) << "popped\n";
	cout << "Top" << peek(root) << endl;
	cout << "elements";

	while (!empty(root))
	{
		cout << peek(root) << "->";
		pop(&root);
	}
	return 0;
}